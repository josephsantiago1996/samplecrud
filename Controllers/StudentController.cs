﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SampleMVC.Models;
using SampleMVC.Models.DBContext;

namespace SampleMVC.Controllers
{
    public class StudentController : Controller
    {
        private StudentDbContext _context;

        public StudentController(StudentDbContext dbContext)
        {
            _context = dbContext;
        }


        public ActionResult Index()
        {
           
            return View(_context.TblStud.ToList());
        }

       
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student std)
        {
            _context.TblStud.Add(std);

            _context.SaveChanges();

            return RedirectToAction("Index");
        }

   
        public ActionResult Edit(int id)
        {
            var std = _context.TblStud.Where(x => x.StudentId == id).FirstOrDefault();
            return View(std);
        }

        [HttpPost]
        public ActionResult Edit(Student std)
        {
            _context.Entry(std).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var std = _context.TblStud.Where(x => x.StudentId == id).FirstOrDefault();

            _context.TblStud.Remove(std);

            _context.SaveChanges();

            return RedirectToAction("Index");
        }

       
    }
}