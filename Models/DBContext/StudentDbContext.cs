﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SampleMVC.Models.DBContext
{
    public class StudentDbContext : DbContext
    {

        public StudentDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Student> TblStud { get; set; }
    }
}
